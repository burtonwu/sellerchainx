﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SellerChain.DTO.Identity.Params
{
    public class RegisterParams
    {
        [Required]
        [EmailAddress]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Company { get; set; }
        public string Phone { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string JobDescription { get; set; }
        [Required]
        public string Mobile { get; set; }
        public string LoginType { get; set; }
        [Required]
        public string DeviceType { get; set; }
        public string IpAddress { get; set; }
        public bool? IsAutoGeneratePwd { get; set; }
        public bool? SkipOtpFlag { get; set; }
        public string ForgotPasswordUrl { get; set; }
        public bool? UseEmailForOtp { get; set; }
    }

}
