﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SellerChain.Data.SQL
{
    public interface ISqlEngine
    {
        List<SqlParameter> AddSqlParameterOutput(string parameter, SqlDbType type, List<SqlParameter> sqlParameters);
        List<SqlParameter> AddSqlParameter(string parameter, DataTable dataTable, List<SqlParameter> sqlParameters);
        List<SqlParameter> AddSqlParameter(string parameter, object value, List<SqlParameter> sqlParameters = null);
        Task<List<T>> ExecuteStoredProcedure<T>(string procedureName, List<SqlParameter> parameters) where T : new();
        Task<List<SqlParameter>> ExecuteStoredProcedure(string procedureName, List<SqlParameter> sqlParameters);
    }
}
