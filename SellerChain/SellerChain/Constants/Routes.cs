﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellerChain.API.Constants
{
    public static class Routes
    {
        public static class User
        {
            public const string Login = "api/user/login";
            public const string Register = "api/user/register";
            public const string Logout = "";
        }
    }
}
